var express = require('express');
var app = express();
var now = new Date();
        pages = require('./pages/pages');

app.configure(function () {
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
});

app.get('/apis/man', pages.getMain);
app.get('/apis/man/:page', pages.getPage);
app.get('/apis/man/:page/:level', pages.getLevelUp);

app.listen(7101);
console.log(now + ': Server listening on 7101');
