var util  = require('util');
	 exec = require('child_process').exec;

exports.getMain = function (req, res) {
	res.send('Man Pages\n');
}

exports.getPage = function (req, res) {
	var page = req.params.page;
	
	mancmd = 'man ' + page;

	manpage = exec(mancmd, function (err, stdout, stderr) {
		if ( stdout !== '' ) {
			res.send('<pre>' + stdout + '</pre>');
		} else if ( stderr.indexOf("No manual entry for") >= 0 )
			{
				res.status(404);
				res.send(stderr);
		}
	});
}

exports.getLevelUp = function (req, res) {
   var page = req.params.page;
   var level = req.params.level;

   mancmd = 'man ' + level + ' ' + page;

   manpage = exec(mancmd, function (err, stdout, stderr) {
      if ( stdout !== '' ) {
         res.send('<pre>' + stdout + '</pre>');
      } else if ( stderr.indexOf("No manual entry for") >= 0 )
         {
            res.status(404);
            res.send(stderr);
      }
   });
}

// Orphaned code:
// -------------|
